import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Book } from '../shared/book.model';
import { BooksService } from '../shared/books.service';
import { Option } from './../../core/shared/option.model';
import { Filter } from './../../core/shared/filter.model';

@Component({
      selector: 'app-book-page',
      templateUrl: './book-page.component.html',
      styleUrls: ['./book-page.component.css']
})
export class BookPageComponent implements OnInit {
      books: Book[];
      options: Option[];
      orderBy: string;
      filter: Filter;

      constructor(private booksService: BooksService,
                  private router: Router) {
        this.options = [
          {text: 'ID', value: 'ID', type: 'number'},
          {text: 'Título', value: 'Title', type: 'text'},
          {text: 'Resumen', value: 'Excerpt', type: 'text'},
          {text: 'Páginas totales', value: 'PageCount', type: 'number'},
          {text: 'Fecha Publicación', value: 'PublishDate', type: 'date'},
        ];
      }

    ngOnInit() {
        this.getBooks();
    }

    getBooks() {
        this.booksService.getbooks()
          .subscribe(books => this.books = books);
    }

    onOrderBy(order: string) {
        this.orderBy = order;
    }

    onFilter(filter: Filter) {
        this.filter = filter;
    }

    newbookForm() {
       this.router.navigateByUrl('books-form/0/0');
    }


}
