import { BooksModule } from './../books.module';
import { Component, OnInit, OnChanges, Input, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
import { Book } from '../shared/book.model';
import { CoreService } from '../../core/shared/core.service';
import { BooksService } from '../shared/books.service';
import { Filter } from './../../core/shared/filter.model';

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.css']
})
export class BookListComponent implements OnInit, OnChanges {
  @Input() books: Book[];
  @Input() orderBy: string;
  @Input() filter: Filter;

  listFilters = [];

  constructor(
    private router: Router,
    private coreService: CoreService,
    private booksService: BooksService
  ) { }

  ngOnInit() {}

  ngOnChanges(changes: SimpleChanges) {
    if (changes.orderBy && changes.orderBy.currentValue) {
      this.coreService.sort(this.books, changes.orderBy.currentValue);
    }

    if (changes.filter && changes.filter.currentValue) {
      this.appyFilters(changes.filter.currentValue);
    }
  }

  appyFilters (newFilter) {
    // Comprobamos si el filtro que entra ya estaba en la lista, si está, se elimina el anterior.
    this.listFilters = this.listFilters.filter (listFilter => listFilter.type !== newFilter.type );

    // Si el filtro es distinto de vacío o mayor que cero, se inserta en la lista.
    if ((parseInt((newFilter.value), 0) > 0 ) || (newFilter.value !== '')) {
      this.listFilters.push(newFilter);
    }

    // Traemos de nuevo una copia de todos los elementos para poder garantizar volver atrás si se elimina
    // un filtro aplicado.
    this.booksService.getbooks()
      .subscribe(books => this.books = this.coreService.filter(books, this.listFilters));

  }
}
