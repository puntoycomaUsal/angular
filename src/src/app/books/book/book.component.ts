import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Book } from './../shared/book.model';
import { BooksService } from './../shared/books.service';
import { Author } from '../../authors/shared/author.model';
import { Coverphoto } from '../../coverphotos/shared/coverphoto.model';

@Component({
    selector: 'app-usuario',
    templateUrl: './book.component.html',
    styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit {

    book        = new Book();
    authors     = new Array<Author>();
    coverphotos = new Array<Coverphoto>();

    constructor(private booksService: BooksService,
                private route: ActivatedRoute) { }

    ngOnInit() {
        this.getBook();
        this.getBookauthors();
        this.getBookcoverphoto();
    }

    getBook() {
        const bookId = +this.route.snapshot.paramMap.get('bookid');
        this.booksService.getbook(bookId)
      . subscribe(book => { this.book = book; });
    }

    getBookauthors() {
        const bookId = +this.route.snapshot.paramMap.get('bookid');
        this.booksService.getbookauthors(bookId)
      . subscribe(authors => { this.authors = authors; });
    }

    getBookcoverphoto() {
        const bookId = +this.route.snapshot.paramMap.get('bookid');
        this.booksService.getbookcoverphoto(bookId)
      . subscribe(coverphotos => { this.coverphotos = coverphotos; });
    }

}
