import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Book } from '../shared/book.model';
import { BooksService } from '../shared/books.service';

@Component({
    selector: 'app-book-summary',
    templateUrl: './book-summary.component.html',
    styleUrls: ['./book-summary.component.css']
})
export class BookSummaryComponent implements OnInit {
    @Input() book: Book;

    constructor(private router: Router, private booksService: BooksService) { }

    ngOnInit (){}

    viewbookDetails (bookId) {
        this.router.navigateByUrl('books/' + bookId);
    }

    editbookForm (bookId , newBook) {
        this.router.navigateByUrl('books-form/' + bookId + '/' + newBook);
    }

    deleteItem (bookId) {
        this.booksService.deletebook(bookId);
        console.log('Se eliminó el registro correctamente pero la API REST no devuelve nada');
        location.reload();
    }

    showSummary = (text, start, end) => text.substring(start, end) + ' ...';
}
