import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Book } from './book.model';
import { Author } from '../../authors/shared/author.model';
import { Coverphoto } from '../../coverphotos/shared/coverphoto.model';

@Injectable()
export class BooksService {

    urlRoot     = 'https://fakerestapi.azurewebsites.net/';
    urlRootApi  = 'https://fakerestapi.azurewebsites.net/api/Books';

    constructor(private http: HttpClient) { }

    getbooks(): Observable<Book[]> {
        return this.http.get<Book[]>(this.urlRootApi);
    }

    getbook(bookId: number): Observable<Book> {
        return this.http.get<Book>(`${this.urlRootApi}/${bookId}`);
    }

    getbookauthors(bookId: number): Observable<Author[]> {
        return this.http.get<Author[]>(`${this.urlRoot}/authors/Books/${bookId}`);
    }

    getbookcoverphoto(bookId: number): Observable<Coverphoto[]> {
        return this.http.get<Coverphoto[]>(`${this.urlRoot}/books/covers/${bookId}`);
    }

    deletebook(bookId: number){
        this.http.delete(`${this.urlRootApi}/${bookId}`);
    }

    editbook(book: Book): Observable<Book> {
        return this.http.put<Book>(`${this.urlRootApi}/${book.ID}`, book);
    }

    newbook(book: Book): Observable<Book> {
        return this.http.post<Book>(this.urlRootApi, book);
    }
}
