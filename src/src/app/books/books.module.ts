import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { AuthorsModule } from '../authors/authors.module';
import { CoverphotosModule} from '../coverphotos/coverphotos.module';
import { CoreModule } from './../core/core.module';

import { BookPageComponent } from './book-page/book-page.component';
import { BookListComponent } from './book-list/book-list.component';
import { BookSummaryComponent } from './book-summary/book-summary.component';
import { BookComponent } from './book/book.component';
import { BookFormComponent } from './book-form/book-form.component';

import { BooksService } from './shared/books.service';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    AuthorsModule,
    CoverphotosModule,
    CoreModule
  ],
  declarations: [BookPageComponent , BookListComponent,
                 BookSummaryComponent, BookComponent,
                 BookFormComponent],
  providers: [BooksService]
})
export class BooksModule { }
