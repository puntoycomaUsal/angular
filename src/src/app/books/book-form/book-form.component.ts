import { Component, OnInit } from '@angular/core';
import { Router , ActivatedRoute } from '@angular/router';
import { Book } from './../shared/book.model';
import { BooksService } from './../shared/books.service';

@Component({
  selector: 'app-usuario',
  templateUrl: './book-form.component.html',
  styleUrls: ['./book-form.component.css']
})
export class BookFormComponent implements OnInit {

  model       = new Book();
  newForm     = 0;

  constructor(private booksService: BooksService,
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
    this.newForm = Number(this.route.snapshot.paramMap.get('newForm'));

    switch (this.newForm){
      case 0:
        //Nuevo
        this.model.PublishDate   = this.getFormatDate('');
      break;
      case 1:
        //Editar
        this.getBook();
      break;
    }

  }

  getBook() {
    const bookId = +this.route.snapshot.paramMap.get('bookid');
    this.booksService.getbook(bookId)
      .subscribe(book => {
        this.model = book;
        this.model.PublishDate   = this.getFormatDate(this.model.PublishDate);
        console.log (this.model);
      });
  }

  submitForm() {
    switch (this.newForm){
      case 0:
        //Nuevo
        this.booksService.newbook(this.model)
        .subscribe(book => {
            alert ("El elemento fue creado correctamente, se muestra en consola la respuesta");
            console.log (book);
            this.router.navigateByUrl('books');
        });
      break;
      case 1:
        //Editar
        this.booksService.editbook(this.model)
        .subscribe(book => {
            alert ("El elemento fue editado correctamente, se muestra en consola la respuesta");
            console.log (book);
            this.router.navigateByUrl('books');
        });
      break;
    }
  }

  getFormatDate (dateStr){
    //Fuente: https://stackoverflow.com/questions/43630445/how-to-convert-current-date-to-yyyy-mm-dd-format-with-angular-2
    let  today  = null;

    if (dateStr != ''){
        today = new Date(dateStr);
    } else {
        today = new Date();
    }

    let dd      = today.getDate();
    let dd_str  = '';
    let mm      = today.getMonth() + 1;
    let mm_str  = '';
    let yyyy    = String (today.getFullYear());

    if(dd<10)
    {
      dd_str    = '0' + String(dd);
    } else {
      dd_str    = String(dd);
    }

    if(mm<10)
    {
      mm_str    = '0' + String(mm);
    } else {
      mm_str    = String(mm);
    }

    return yyyy+'-'+mm_str+'-'+dd_str;
  }
}
