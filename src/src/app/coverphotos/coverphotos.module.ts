import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { CoreModule } from './../core/core.module';

import { CoverphotoPageComponent } from './coverphoto-page/coverphoto-page.component';
import { CoverphotoListComponent } from './coverphoto-list/coverphoto-list.component';
import { CoverphotoSummaryComponent } from './coverphoto-summary/coverphoto-summary.component';
import { CoverphotoComponent } from './coverphoto/coverphoto.component';
import { CoverphotoFormComponent } from './coverphoto-form/coverphoto-form.component';

import { CoverphotosService } from './shared/coverphotos.service';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    CoreModule
  ],
  declarations: [CoverphotoPageComponent , CoverphotoListComponent,
                 CoverphotoSummaryComponent, CoverphotoComponent,
                 CoverphotoFormComponent],
  providers: [CoverphotosService],
  exports:[CoverphotoListComponent]
})
export class CoverphotosModule { }
