import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Coverphoto } from './coverphoto.model';

@Injectable()
export class CoverphotosService {
  
    urlRoot     = 'https://fakerestapi.azurewebsites.net/';
    urlRootApi  = 'https://fakerestapi.azurewebsites.net/api/CoverPhotos';

    constructor(private http: HttpClient) { }

    getcoverphotos(): Observable<Coverphoto[]> {
        return this.http.get<Coverphoto[]>(this.urlRootApi);
    }

    getcoverphoto(coverphotoId: number): Observable<Coverphoto> {
        return this.http.get<Coverphoto>(`${this.urlRootApi}/${coverphotoId}`);
    }

    deletecoverphoto(coverphotoId: number){
        this.http.delete(`${this.urlRootApi}/${coverphotoId}`);
    }

    editcoverphoto(coverphoto: Coverphoto): Observable<Coverphoto> {
        return this.http.put<Coverphoto>(`${this.urlRootApi}/${coverphoto.ID}`, coverphoto);
    }

    newcoverphoto(coverphoto: Coverphoto): Observable<Coverphoto> {
        return this.http.post<Coverphoto>(this.urlRootApi, coverphoto);
    }
}
