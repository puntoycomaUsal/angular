import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Coverphoto } from './../shared/coverphoto.model';
import { CoverphotosService } from './../shared/coverphotos.service';

@Component({
  selector: 'app-usuario',
  templateUrl: './coverphoto.component.html',
  styleUrls: ['./coverphoto.component.css']
})
export class CoverphotoComponent implements OnInit {

  coverphoto  = new Coverphoto();

  constructor(private coverphotosService: CoverphotosService,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.getCoverphoto();
  }

  getCoverphoto() {
    const coverphotoId = +this.route.snapshot.paramMap.get('coverphotoid');
    this.coverphotosService.getcoverphoto(coverphotoId)
      .subscribe(coverphoto => { this.coverphoto = coverphoto; });
  }

}
