import { Component, OnInit, OnChanges, Input, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
import { Coverphoto } from '../shared/coverphoto.model';
import { CoreService } from '../../core/shared/core.service';
import { CoverphotosService } from '../shared/coverphotos.service';
import { Filter } from './../../core/shared/filter.model';

@Component({
  selector: 'app-coverphoto-list',
  templateUrl: './coverphoto-list.component.html',
  styleUrls: ['./coverphoto-list.component.css']
})
export class CoverphotoListComponent implements OnInit, OnChanges {
  @Input() coverphotos: Coverphoto[];
  @Input() orderBy: string;
  @Input() filter: Filter;

  listFilters = [];

  constructor(
    private router: Router,
    private coreService: CoreService,
    private coverphotosService: CoverphotosService
  ) { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.orderBy && changes.orderBy.currentValue) {
      this.coreService.sort(this.coverphotos, changes.orderBy.currentValue);
    }

    if (changes.filter && changes.filter.currentValue) {
      this.appyFilters(changes.filter.currentValue);
    }
  }

  appyFilters (newFilter) {
    // Comprobamos si el filtro que entra ya estaba en la lista, si está, se elimina el anterior.
    this.listFilters = this.listFilters.filter (listFilter => listFilter.type !== newFilter.type );

    // Si el filtro es distinto de vacío o mayor que cero, se inserta en la lista.
    if ((parseInt((newFilter.value), 0) > 0 ) || (newFilter.value !== '')) {
      this.listFilters.push(newFilter);
    }

    // Traemos de nuevo una copia de todos los elementos para poder garantizar volver atrás si se elimina
    // un filtro aplicado.
    this.coverphotosService.getcoverphotos()
      .subscribe(coverphotos => this.coverphotos = this.coreService.filter(coverphotos, this.listFilters));
  }
}
