import { Component, OnInit } from '@angular/core';
import { Router , ActivatedRoute } from '@angular/router';
import { Coverphoto } from './../shared/coverphoto.model';
import { CoverphotosService } from './../shared/coverphotos.service';

@Component({
  selector: 'app-usuario',
  templateUrl: './coverphoto-form.component.html',
  styleUrls: ['./coverphoto-form.component.css']
})
export class CoverphotoFormComponent implements OnInit {

  model       = new Coverphoto();
  newForm     = 0;

  constructor(private coverphotosService: CoverphotosService,
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
    this.newForm = Number(this.route.snapshot.paramMap.get('newForm'));

    switch (this.newForm){
      //case 0:
        //Nuevo
        //this.model.PublishDate   = this.getFormatDate('');
      //break;
      case 1:
        //Editar
        this.getCoverphoto();
      break;
    }

  }

  getCoverphoto() {
    const coverphotoId = +this.route.snapshot.paramMap.get('coverphotoid');
    this.coverphotosService.getcoverphoto(coverphotoId)
      .subscribe(coverphoto => {
        this.model = coverphoto;
        //this.model.PublishDate   = this.getFormatDate(this.model.PublishDate);
        console.log (this.model);
      });
  }

  submitForm() {
    switch (this.newForm){
      case 0:
        //Nuevo
        this.coverphotosService.newcoverphoto(this.model)
        .subscribe(coverphoto => {
            alert ("El elemento fue creado correctamente, se muestra en consola la respuesta");
            console.log (coverphoto);
            this.router.navigateByUrl('coverphotos');
        });
      break;
      case 1:
        //Editar
        this.coverphotosService.editcoverphoto(this.model)
        .subscribe(coverphoto => {
            alert ("El elemento fue editado correctamente, se muestra en consola la respuesta");
            console.log (coverphoto);
            this.router.navigateByUrl('coverphotos');
        });
      break;
    }
  }
}
