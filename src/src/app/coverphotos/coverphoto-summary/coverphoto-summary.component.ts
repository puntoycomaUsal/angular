import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Coverphoto } from '../shared/coverphoto.model';
import { CoverphotosService } from '../shared/coverphotos.service';

@Component({
    selector: 'app-coverphoto-summary',
    templateUrl: './coverphoto-summary.component.html',
    styleUrls: ['./coverphoto-summary.component.css']
})
export class CoverphotoSummaryComponent implements OnInit {
    @Input() coverphoto: Coverphoto;

    constructor(private router: Router, private coverphotosService: CoverphotosService) { }

    ngOnInit (){}

    viewcoverphotoDetails (coverphotoId) {
        this.router.navigateByUrl('coverphotos/' + coverphotoId);
    }

    editcoverphotoForm (coverphotoId , newCoverphoto) {
        this.router.navigateByUrl('coverphotos-form/' + coverphotoId + '/' + newCoverphoto);
    }

    deleteItem (coverphotoId) {
        this.coverphotosService.deletecoverphoto(coverphotoId);
        console.log('Se eliminó el registro correctamente pero la API REST no devuelve nada');
        location.reload();
    }

    showSummary = (text, start, end) => text.substring(start, end) + ' ...';
}
