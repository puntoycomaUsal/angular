import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Coverphoto } from '../shared/coverphoto.model';
import { CoverphotosService } from '../shared/coverphotos.service';
import { Option } from './../../core/shared/option.model';
import { Filter } from './../../core/shared/filter.model';

@Component({
      selector: 'app-coverphoto-page',
      templateUrl: './coverphoto-page.component.html',
      styleUrls: ['./coverphoto-page.component.css']
})
export class CoverphotoPageComponent implements OnInit {
      coverphotos: Coverphoto[];
      options: Option[];
      orderBy: string;
      filter: Filter;

      constructor(private coverphotosService: CoverphotosService,
                  private router: Router) {
        this.options = [
          {text: 'ID', value: 'ID', type: 'number'},
          {text: 'Libro ID', value: 'IDBook', type: 'number'},
          {text: 'Url', value: 'Url' , type: 'url'},
        ];
      }

    ngOnInit() {
        this.getCoverphotos();
    }

    getCoverphotos() {
        this.coverphotosService.getcoverphotos()
          .subscribe(coverphotos => this.coverphotos = coverphotos);
    }

    onOrderBy(order: string) {
        this.orderBy = order;
    }

    onFilter(filter: Filter) {
      this.filter = filter;
    }

    newcoverphotoForm() {
      this.router.navigateByUrl('coverphotos-form/0/0');
    }


}
