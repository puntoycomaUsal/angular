import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { Option } from '../shared/option.model';
import { Filter } from '../shared/filter.model';

@Component({
    selector: 'app-filter',
    templateUrl: './filter.component.html',
    styleUrls: ['./filter.component.css']
})
export class FilterComponent implements OnInit {
    @Input()  options: Option[];
    @Output() order = new EventEmitter<string>();
    @Output() filter = new EventEmitter<Filter>();

  constructor() { }

  ngOnInit() {
  }

  onChangeOrderBy(event: any) {
    this.order.emit(event.target.value);
  }

  onFilter(field, type, event: any) {
    const filterData  = new Filter();
    filterData.field  = field;
    filterData.type   = type;
    filterData.value  = event.target.value;
    this.filter.emit(filterData);
  }

}
