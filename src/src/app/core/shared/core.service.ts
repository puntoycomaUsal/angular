import { Injectable } from '@angular/core';
import { Filter } from './filter.model';

@Injectable()
export class CoreService {

    constructor() { }

    sort(array: any[], key: string) {
      return array.sort((a, b) => {
        return a[key] > b[key] ? 1 : -1;
      });
    }

    filter(models: any[], listFilters: Filter[]) {
        listFilters.forEach(listFilter => {
        switch (listFilter.type) {
            case 'number':
                models = models.filter (model => model[listFilter.field] === parseInt(listFilter.value, 0 ));
            break;

            case 'date':
            console.log(listFilter);
            models = models.filter (model => this.formatDate(model[listFilter.field]) === listFilter.value);
            break;

            default:
                // Para buscar en los campos de texto se hará a través de una expresión regular, con el parámetro i
                // para que sea "case insensitive" es decir que no distinga entre mayúsculas y minúsculas.
                models = models.filter (model => model[listFilter.field].match(new RegExp(listFilter.value, 'i')) !== null);
            break;
        }
      });
      return models;
    }

    formatDate (dateTime) {
        const date  = new Date(dateTime);
        const yyyy  = date.getFullYear().toString();
        let mm    = (date.getMonth() + 1).toString();
        if (mm.length < 2) {
            mm = '0' + mm;
        }
        let dd    = date.getDate().toString();
        if (dd.length < 2) {
            dd = '0' + dd;
        }
        console.log (yyyy + '-' + mm + '-' + dd);
        return(yyyy + '-' + mm + '-' + dd);
    }

}
