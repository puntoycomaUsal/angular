export class Option {
  value: string;
  text: string;
  type: string;
}
