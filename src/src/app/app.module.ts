import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { MenuModule } from './menu/menu.module';
import { BooksModule } from './books/books.module';
import { AuthorsModule } from './authors/authors.module';
import { CoverphotosModule } from './coverphotos/coverphotos.module';
import { PagesModule } from './pages/pages.module';
import { CoreModule } from './core/core.module';

import { AppComponent } from './app.component';
import { MenuComponent } from './menu/menu/menu.component';
import { IndexPageComponent } from './pages/index-page/index-page.component';

@NgModule({
    declarations: [
      AppComponent,
      MenuComponent,
      IndexPageComponent,
    ],
    imports: [
      BrowserModule,
      AppRoutingModule,
      MenuModule,
      PagesModule,
      BooksModule,
      AuthorsModule,
      CoverphotosModule,
      CoreModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
