import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IndexPageComponent } from './pages/index-page/index-page.component';
import { BookPageComponent } from './books/book-page/book-page.component';
import { BookComponent } from './books/book/book.component';
import { BookFormComponent } from './books/book-form/book-form.component';
import { AuthorPageComponent } from './authors/author-page/author-page.component';
import { AuthorComponent } from './authors/author/author.component';
import { AuthorFormComponent } from './authors/author-form/author-form.component';
import { CoverphotoPageComponent } from './coverphotos/coverphoto-page/coverphoto-page.component';
import { CoverphotoComponent } from './coverphotos/coverphoto/coverphoto.component';
import { CoverphotoFormComponent } from './coverphotos/coverphoto-form/coverphoto-form.component';

const routes: Routes = [
  { path: '', component: IndexPageComponent },
  { path: 'books', component: BookPageComponent },
  { path: 'books/:bookid', component: BookComponent },
  { path: 'books-form/:bookid/:newForm', component: BookFormComponent },
  { path: 'authors', component: AuthorPageComponent },
  { path: 'authors/:authorid', component: AuthorComponent },
  { path: 'authors-form/:authorid/:newForm', component: AuthorFormComponent },
  { path: 'coverphotos', component: CoverphotoPageComponent },
  { path: 'coverphotos/:coverphotoid', component: CoverphotoComponent },
  { path: 'coverphotos-form/:coverphotoid/:newForm', component: CoverphotoFormComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
