import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Author } from './../shared/author.model';
import { AuthorsService } from './../shared/authors.service';

@Component({
  selector: 'app-usuario',
  templateUrl: './author.component.html',
  styleUrls: ['./author.component.css']
})
export class AuthorComponent implements OnInit {

  author  = new Author();

  constructor(private booksService: AuthorsService,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.getAuthor();
  }

  getAuthor() {
    const authorId =+ this.route.snapshot.paramMap.get('authorid');
    this.booksService.getauthor(authorId)
      .subscribe(author => { this.author = author; });
  }

}
