import { Component, OnInit } from '@angular/core';
import { Router , ActivatedRoute } from '@angular/router';
import { Author } from './../shared/author.model';
import { AuthorsService } from './../shared/authors.service';

@Component({
  selector: 'app-usuario',
  templateUrl: './author-form.component.html',
  styleUrls: ['./author-form.component.css']
})
export class AuthorFormComponent implements OnInit {

  model       = new Author();
  newForm     = 0;

  constructor(private authorsService: AuthorsService,
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
    this.newForm = Number(this.route.snapshot.paramMap.get('newForm'));

    switch (this.newForm){
      case 1:
        //Editar
        this.getAuthor();
      break;
    }

  }

  getAuthor() {
    const authorId = +this.route.snapshot.paramMap.get('authorid');
    this.authorsService.getauthor(authorId)
      .subscribe(author => {
        this.model = author;
        console.log (this.model);
      });
  }

  submitForm() {
    switch (this.newForm){
      case 0:
        //Nuevo
        this.authorsService.newauthor(this.model)
        .subscribe(author => {
            alert ("El elemento fue creado correctamente, se muestra en consola la respuesta");
            console.log (author);
            this.router.navigateByUrl('authors');
        });
      break;
      case 1:
        //Editar
        this.authorsService.editauthor(this.model)
        .subscribe(author => {
            alert ("El elemento fue editado correctamente, se muestra en consola la respuesta");
            console.log (author);
            this.router.navigateByUrl('authors');
        });
      break;
    }
  }
}
