import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Author } from './author.model';

@Injectable()
export class AuthorsService {

    urlRoot     = 'https://fakerestapi.azurewebsites.net/';
    urlRootApi  = 'https://fakerestapi.azurewebsites.net/api/Authors';

  constructor(private http: HttpClient) { }

  getauthors(): Observable<Author[]> {
    return this.http.get<Author[]>(this.urlRootApi);
  }

  getauthor(authorId: number): Observable<Author> {
    return this.http.get<Author>(`${this.urlRootApi}/${authorId}`);
  }

  deleteauthor(authorId: number): Observable<string> {
    return this.http.delete<string>(`${this.urlRootApi}/${authorId}`);
  }

  editauthor(author: Author): Observable<Author> {
    return this.http.put<Author>(`${this.urlRootApi}/${author.ID}`, author);
  }

  newauthor(author: Author): Observable<Author> {
    return this.http.post<Author>(this.urlRootApi, author);
  }
}
