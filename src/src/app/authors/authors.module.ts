import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { CoreModule } from './../core/core.module';

import { AuthorPageComponent } from './author-page/author-page.component';
import { AuthorListComponent } from './author-list/author-list.component';
import { AuthorSummaryComponent } from './author-summary/author-summary.component';
import { AuthorComponent } from './author/author.component';
import { AuthorFormComponent } from './author-form/author-form.component';

import { AuthorsService } from './shared/authors.service';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    CoreModule
  ],
  declarations: [AuthorPageComponent , AuthorListComponent,
                 AuthorSummaryComponent, AuthorComponent,
                 AuthorFormComponent],
  providers: [AuthorsService],
  exports:[AuthorListComponent]
})
export class AuthorsModule { }
