import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Author } from '../shared/author.model';
import { AuthorsService } from '../shared/authors.service';

@Component({
    selector: 'app-author-summary',
    templateUrl: './author-summary.component.html',
    styleUrls: ['./author-summary.component.css']
})
export class AuthorSummaryComponent implements OnInit {
    @Input() author: Author;

    constructor(private router: Router, private authorsService: AuthorsService) { }

    ngOnInit() {}

    viewauthorDetails(authorId){
        this.router.navigateByUrl('authors/' + authorId);
    }

    editauthorForm(authorId , newAuthor) {
        this.router.navigateByUrl('authors-form/' + authorId + '/' + newAuthor);
    }

    deleteItem(authorId) {
      this.authorsService.deleteauthor(authorId)
      console.log('Se eliminó el registro correctamente pero la API REST no devuelve nada');
      location.reload();
    }

}
