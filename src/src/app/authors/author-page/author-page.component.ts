import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Author } from '../shared/author.model';
import { AuthorsService } from '../shared/authors.service';
import { Option } from '../../core/shared/option.model';
import { Filter } from './../../core/shared/filter.model';

@Component({
      selector: 'app-author-page',
      templateUrl: './author-page.component.html',
      styleUrls: ['./author-page.component.css']
})
export class AuthorPageComponent implements OnInit {
      authors: Author[];
      options: Option[];
      orderBy: string;
      filter: Filter;

      constructor(private authorsService: AuthorsService,
                  private router: Router) {
        this.options = [
          {text: 'ID', value: 'ID', type: 'number'},
          {text: 'Libro ID', value: 'IDBook' , type: 'number'},
          {text: 'Nombre', value: 'FirstName', type: 'text'},
          {text: 'Apellidos', value: 'LastName', type: 'text'},
        ];
      }

    ngOnInit() {
        this.getAuthors();
    }

    getAuthors() {
        this.authorsService.getauthors()
          .subscribe(authors => this.authors = authors);
    }

    onOrderBy(order: string) {
        this.orderBy = order;
    }

    onFilter(filter: Filter) {
      this.filter = filter;
    }
    newauthorForm() {
      this.router.navigateByUrl('authors-form/0/0');
    }


}
